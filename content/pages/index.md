Title: ALAR
Date: 2017-01-12 20:00
Modified: 2017-01-12 20:00
Slug: historia
Authors: Martin Beracochea
Summary: Historia de la Asociación Latinoamericana de Rizobiología
save_as: index.html

<div style="text-align: center;">
    <img src="/images/logo.png">
</div>

# Historia

La Asociación  Latinoamericana de Rizobiología  (ALAR ) es una  asociación científica sin fines de lucro cuyo objetivo es promover  las actividades de fijación biológica de nitrógeno  y el  uso de los microorganismos promotores del crecimiento vegetal  en los países de América Latina  y Caribe.

Una de sus principales  actividades ha sido  la coordinación de las  Reuniones Latinoamericanas de Rizobiología (RELAR) cuyo objetivo es mantener un ámbito  de  actualización e intercambio de experiencias y  conocimientos de los  aspectos básicos y aplicados del uso de esta tecnología.  La RELAR se ha transformado en un foro internacional significativo y en una excelente oportunidad para jóvenes investigadores.

Los orígenes de la ALAR se remontan  a la década del 60,  su fundación  ocurre en 1964  durante la I RELAR en Montevideo, Uruguay y se formaliza en  la X RELAR en Maracay, Venezuela, en 1980.

En el 2009 se realizó la primera reunión conjunta ALAR-SEFIN (Sociedad Española de Fijación Biológica del Nitrógeno), con la convocatoria a la XXIV RELAR y I IBEMPA (Conferencia Iberoamericana de interacciones beneficiosas microorganismo-planta-ambiente), lo que representó un significativo avance en los objetivos de ambas sociedades. Estas reuniones conjuntas se reiterarán cada 4 años.

La estructura organizativa actual esta formada por una Secretaría Ejecutiva con sede en Montevideo, Uruguay   y  un Presidente  y Vice-Presidente que  alternan en función de las  RELAR.

Las  actividades han sido financiadas por la Organización de las Naciones Unidas para la Agricultura y la Alimentación (FAO) hasta 1994,   Ministerio de Ganadería Agricultura y Pesca (MGAP), Uruguay, hasta 2006 y BIAGRO desde 2000.

