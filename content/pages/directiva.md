Title: Directiva
Date: 2017-01-12 20:00
Modified: 2017-01-12 20:00
Slug: directiva
Authors: Martin Beracochea
Summary: Directiva de la Asociación Latinoamericana de Rizobiología


# Directiva

Presidenta : Doris Zúñiga, Universidad Nacional Agraria La Molina, Lima-Peru.

Vicepresidenta : Mariangela Hungría.  Embrapa Centro Nacional de Pesquisa de Soja. Brasil.

Secretaria Ejecutiva : Elena Fabiano. IIBCE, Uruguay.

Secretario : Federico Battistoni. IIBCE, Uruguay.

Tesorera : Natalia Bajsa. IIBCE, Uruguay.

## Coordinadores Regionales

Argentina. Adriana Fabra (afabra@exa.unrc.edu.ar) y Hernán Lascano (hrlascano@correo.inta.gov.ar).

Brasil. Verónica Reis (veronica@cnpab.embrapa.br)  y Mariangela Hungría (mariangelahungria@hotmail.com).

Bolivia. Renato Valenzuela (renato@fertimax.com).

Chile. Maribel Parada (mparada@ufro.cl).

Colombia. Nubia Moreno (ncmorenos@unal.edu.co).

Cuba. María Caridad Nápoles (tere@inca.edu.cu) y Eduardo Ortega (eortega@fq.uh.cu).

Ecuador. Carlos Zambrano (czcarriel@yahoo.es).

Paraguay. Pilar Galeano (pilar.galeano@agr.una.py).

Perú. Doris Zúñiga (dzuniga@lamolina.edu.pe).

Uruguay. Alicia Arias (ariaslefort@gmail.com), Elena Beyhaut  (ebeyhaut@inia.org.uy) y Elena Fabiano(elena.fabiano@gmail.com).

Venezuela. Marcia Toro (marcia.toro@ciens.ucv.ve).

México. Esperanza Martínez-Romero (emartine@ccg.unam.mx) y Pablo Vinuesa (pvinuesa.mex1@gmail.com).
