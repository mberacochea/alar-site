#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Secretaria Alar'
SITENAME = 'Asociación Latinoamericana de Rizobiología'
SITEURL = 'www.alaronline.org'

PATH = 'public'
THEME = 'aboutwilson'
TIMEZONE = 'America/Montevideo'

OUTPUT_PATH = 'public'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = ()

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

STATIC_PATHS = [ 'images', 'static/robots.txt' ]

EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
}

# Analtics
GOOGLE_ANALYTICS = 'UA-37209269-1'